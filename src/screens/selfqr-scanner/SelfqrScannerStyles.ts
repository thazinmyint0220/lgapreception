import { StyleSheet } from "react-native";
import { colors } from "../../styles/color";

export const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
  },
  container: {
    flexDirection: "row",
    flex: 1,
    backgroundColor: "#f5f5f5",
  },
  leftSide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  rightSide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  overlay: {
    //temporary add
    position: "absolute",
    bottom: 20,
    // ...StyleSheet.absoluteFillObject,
    // backgroundColor: "rgba(0, 0, 0, 0.5)",
    zIndex: 1,
  },
  //temporary add buttonContainer, receptionByOne, receptionByFamily
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: 597.2,
  },
  receptionByOne: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderWidth: 1,
    backgroundColor: colors.secondary,
  },
  receptionByFamily: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderWidth: 1,
    backgroundColor: colors.secondary,
  },
  camera: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: [{ translateX: -180 }, { translateY: -180 }],
    height: 360,
    width: 360,
    zIndex: 1,
  },
  corner: {
    position: "absolute",
    width: 30,
    height: 30,
    borderWidth: 3,
    borderRadius: 5,
    zIndex: 2,
    borderColor: "transparent",
  },

  topLeftCorner: {
    top: "50%",
    left: "50%",
    marginTop: -182,
    marginLeft: -182,
    borderTopColor: "white",
    borderLeftColor: "white",
  },
  topRightCorner: {
    top: "50%",
    left: "50%",
    marginTop: -182,
    marginLeft: 152,
    borderTopColor: "white",
    borderRightColor: "white",
  },
  bottomLeftCorner: {
    top: "50%",
    left: "50%",
    marginTop: 152,
    marginLeft: -182,
    borderBottomColor: "white",
    borderLeftColor: "white",
  },
  bottomRightCorner: {
    top: "50%",
    left: "50%",
    marginTop: 152,
    marginLeft: 152,
    borderBottomColor: "white",
    borderRightColor: "white",
  },
  image: {
    width: "100%",
    height: "80%",
    resizeMode: "contain",
    margin: 10,
  },
});
