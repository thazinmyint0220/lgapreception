import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  Text,
  Alert,
  Pressable,
} from "react-native";
import { Camera } from "expo-camera";
import { Header } from "../../components/basics/header";
import { Footer } from "../../components/basics/footer";
import { styles } from "./SelfqrScannerStyles";
import { NavigationProp } from "@react-navigation/native";
import { HiraginoKakuText } from "../../components/StyledText";
import QRCodeScanner from 'react-native-qrcode-scanner';

type Props = {
  navigation: NavigationProp<any, any>;
};

export const SelfqrScanner = ({ navigation }: Props) => {
  const [hasPermission, setHasPermission] = useState<boolean | null>(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const handleBarcodeScanned = ({ data }: { data: string }) => {
    if (!scanned) {
      setScanned(true);
      console.log("qr data : ", data);
      Alert.alert(
        "QR Code Scanned",
        `Bar code with data ${data} has been scanned!`,
        [
          {
            text: "OK",
            onPress: () => {
              setScanned(false);
            },
          },
        ],
        { cancelable: false }
      );
    }
  };

  if (hasPermission === null) {
    return <Text>Requesting camera permission...</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  const handleReturnButton = () => {
    navigation.navigate("SelfqrDescription", {
      userId: "mec",
    });
  };

  const handleCheckInConfirmation = () => {
    navigation.navigate("CheckInConfirmation", {
      userId: "mec",
    });
  };

  const handleGroupCheckInConfirmation = () => {
    navigation.navigate("GroupCheckInConfirmation", {
      userId: "mec",
    });
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="自己QRをかざしてください"
        buttonName="受付をやめる"
        onPress={handleSelectReceptionMethod}
      />
      <View style={styles.container}>
        <View style={styles.leftSide}>
          <Image
            source={require("../../assets/images/qrScanner.png")}
            style={styles.image}
          />
        </View>
        <View style={styles.rightSide}>
          <View style={styles.camera}></View>
          {hasPermission === true && (
            // <Camera
            //   style={styles.camera}
            //   type={"back" as any}
            //   onBarCodeScanned={handleBarcodeScanned}
            // />
            <QRCodeScanner
              cameraContainerStyle={styles.camera}
              reactivate={true}
              showMarker={true}
              onRead={handleBarcodeScanned}
            />
          )}
          <View style={[styles.corner, styles.topLeftCorner]} />
          <View style={[styles.corner, styles.topRightCorner]} />
          <View style={[styles.corner, styles.bottomLeftCorner]} />
          <View style={[styles.corner, styles.bottomRightCorner]} />
          <View style={styles.overlay}>
            <View style={styles.buttonContainer}>
              <Pressable
                style={styles.receptionByOne}
                onPress={handleCheckInConfirmation}
              >
                <HiraginoKakuText normal>1人で受付</HiraginoKakuText>
              </Pressable>
              <Pressable
                style={styles.receptionByFamily}
                onPress={handleGroupCheckInConfirmation}
              >
                <HiraginoKakuText normal>家族で受付</HiraginoKakuText>
              </Pressable>
            </View>
          </View>
        </View>
      </View>
      <Footer
        hasNextButton={false}
        onPressPrevious={handleReturnButton}
      ></Footer>
    </SafeAreaView>
  );
};
